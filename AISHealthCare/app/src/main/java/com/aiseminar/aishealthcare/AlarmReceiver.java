package com.aiseminar.aishealthcare;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AlarmReceiver extends BroadcastReceiver {
    public AlarmReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (SleepFragment.ACTION_SLEEP_ALARM.equals(intent.getAction())) {
            SoundManager.getInstance().setContext(context);
            SoundManager.getInstance().playSleepAlarm();
        }
    }
}
