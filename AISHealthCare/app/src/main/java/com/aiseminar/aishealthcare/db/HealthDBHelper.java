package com.aiseminar.aishealthcare.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by rdc-hankang on 4/9/15.
 */
public class HealthDBHelper extends SQLiteOpenHelper {
    private static final String dbName = "AISHealth.db";
    private static final Integer dbVersion = 1;

    private SQLiteDatabase healthDB;

    HealthDBHelper(Context appContext) {
        super(appContext, dbName, null, dbVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sql = "CREATE TABLE `ais_push_ups` (" +
                "  `id` int(11) unsigned NOT NULL AUTO_INCREMENT," +
                "  `time` datetime NOT NULL," +
                "  `push_times` int(11) NOT NULL," +
                "  PRIMARY KEY (`id`)" +
                ");";
        sqLiteDatabase.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
