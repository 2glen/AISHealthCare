package com.aiseminar.aishealthcare;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.Calendar;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SleepFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SleepFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SleepFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    public static final String ACTION_SLEEP_ALARM = "com.aiseminar.aishealthcare.action.alarm";
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Button btnStartSleep = null;
    private TimePicker tpTimeLength = null;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SleepFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SleepFragment newInstance(String param1, String param2) {
        SleepFragment fragment = new SleepFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public SleepFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_sleep, container, false);
        btnStartSleep = (Button)(rootView.findViewById(R.id.btnStartSleep));
        btnStartSleep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SleepFragment.this.onBtnStartSleepClicked();
            }
        });
        tpTimeLength = (TimePicker)(rootView.findViewById(R.id.timePicker));
        tpTimeLength.setIs24HourView(true);
        tpTimeLength.setCurrentHour(8);
        tpTimeLength.setCurrentMinute(0);

        return rootView;
    }

    public void onBtnStartSleepClicked() {
        long timeInMillis = (tpTimeLength.getCurrentHour() * 60 + tpTimeLength.getCurrentMinute()) * 60 * 1000;

        setAlarmTime(timeInMillis);
        btnStartSleep.setText(tpTimeLength.getCurrentHour().toString() + "小时" + tpTimeLength.getCurrentMinute().toString() + "分钟后叫醒");
    }

    private void setAlarmTime(long timeInMillis) {
        AlarmManager am = (AlarmManager)this.getActivity().getApplicationContext().getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(ACTION_SLEEP_ALARM);
        PendingIntent sender = PendingIntent.getBroadcast(this.getActivity().getApplicationContext(), 0, intent, 0);

        am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + timeInMillis, sender);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
