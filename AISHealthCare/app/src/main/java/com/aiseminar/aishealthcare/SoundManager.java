package com.aiseminar.aishealthcare;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.media.SoundPool;
import android.os.Build;

/**
 * Created by rdc-hankang on 4/15/15.
 */
public class SoundManager {
    private static SoundManager ourInstance = new SoundManager();

    public static SoundManager getInstance() {
        return ourInstance;
    }

    private SoundPool soundPool = null;
    private RingtoneManager ringtoneManager = null;

    private Context context = null;

    private SoundManager() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            SoundPool.Builder builder = new SoundPool.Builder();
            soundPool = builder.build();
        } else {
            soundPool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        }
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void loadRes() {
//        soundPool.load(getContext(), R.raw.s1, 1);
    }

    public void playPushUpSound() {
        MediaPlayer mediaPlayer = null;
        mediaPlayer = MediaPlayer.create(getContext(), R.raw.s1);
        mediaPlayer.start();
    }

    public void playSleepAlarm() {
        MediaPlayer mediaPlayer = null;
        mediaPlayer = MediaPlayer.create(getContext(), RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM));
        mediaPlayer.start();
    }


}
